<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

  <div class="container">
      <?php include('topbar.php');?>
  </div>
<div class="container">
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6"> <br />
      <h4 align="center"> รหัสการสั่งซื้อ : <?php echo $_GET['id'];?> </h4>
      <hr />
      <form action="add_confirm.php?id=<?php echo $_GET['id'];?>" method="POST" enctype="multipart/form-data"  name="addprd" class="form-horizontal" id="addprd">
        <div class="form-group">
          <div class="col-sm-8 info">
            <input type="file" name="p_img" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <a href="searchEmpForm.php"><button type="button" name="button" id="btnprint2" class="btn btn btn-warning" >กลับ</button></a>
            <button type="submit" class="btn btn-primary" name="btnadd"> ยื่นยัน </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>
