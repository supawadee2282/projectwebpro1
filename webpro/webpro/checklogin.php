<?php
	//Start session
	session_start();
	include('dbConfig.php');
	//Array to store validation errors
	$errmsg_arr = array();

	//Validation error flag
	$errflag = false;


	//Function to sanitize values received from the form. Prevents SQL injection


	//Sanitize the POST values
	$login = mysqli_real_escape_string($db,$_POST['username']);
	$password = mysqli_real_escape_string($db,$_POST['password']);
	$passwordHas = $_POST['password'];
	//Input Validations
	if($login == '') {
		$errmsg_arr[] = 'Username missing';
		$errflag = true;
	}
	if($passwordHas == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}

	//If there are input validations, redirect back to the login form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location:useradmin.php");
		exit();
	}
	//Create query
	$qry=" select * from member where m_username = '$login' and password = '$passwordHas' ";
	$result=mysqli_query($db,$qry);

	//Check whether the query was successful or not
	if($result) {
		if(mysqli_num_rows($result) > 0) {
			//Login Successful
			session_regenerate_id();
			$data = mysqli_fetch_assoc($result);

			$_SESSION['id_member']=$data['id_member'];
			$_SESSION['m_username']=$data['m_username'];
			$_SESSION['password']=$data['password'];
			$_SESSION['level']=$data['level'];

			session_write_close();
			if($_SESSION['level']=="1")
			{
				header("location:useradmin.php");
			}
			else if($_SESSION['level']=="2" ){
				header("location:staff.php");
			}
			else if($_SESSION['level']=="3" ){
				header("location:index.php");				 					
			}
		}
		else {
			//Login failed
			echo"<script type='text/javascript'>alert ('เข้าสู่ระบบไม่ได้ เนื่องจากชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')</script>";
			echo "<meta http-equiv='refresh' content='0;url=connectMem.php'>";

			exit();
		}
	}else {
		die("Query failed");
	}

?>
