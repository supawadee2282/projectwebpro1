// JavaScript Document
var xmlhttp;
function loadXMLDocGet(url,cfunc){

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=cfunc;
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
}

function loadXMLDocPost(url,para,cfunc){

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=cfunc;
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(para);
}

function displayEmpPOST(){
	var name = document.getElementById("packing_no").value;
	if(name=="")
		alert("please input name");
	else{
		var para = "n="+name;
		loadXMLDocPost("order_now.php",para,function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200)
				document.getElementById("showData").innerHTML= xmlhttp.responseText;
			else
				document.getElementById("showData").innerHTML= "waiting";
		} );
	}
}

function displayEmpGET(){
	var name = document.getElementById("name").value;
	if(name=="")
		alert("please input name");
	else{
		loadXMLDocGet("displayEmp.php?n="+name,function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200)
				document.getElementById("showData").innerHTML= xmlhttp.responseText;
			else
				document.getElementById("showData").innerHTML= "waiting";
		} );
	}
}


function showHello(){
	var name = document.getElementById("name").value;
	var age = document.getElementById("age").value;
	if(name==""||age=="")
		alert("please input all fields");
	else{
		loadXMLDocGet("hello.php?n="+name+"&a="+age,function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200)
				document.getElementById("showData").innerHTML= xmlhttp.responseText;


		});
	}
}






function loadXMLDocPost(url,para,cfunc){

	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=cfunc;
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(para);
}


function insertDept(){
	var id = document.getElementById("did").value;
	var name = document.getElementById("name").value;
	loadXMLDoc("insertDept.php?id="+id+"&name="+name,function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("listData").innerHTML=xmlhttp.responseText;
		}else{
			document.getElementById("listData").innerHTML="waiting..";
		}

	});
}
