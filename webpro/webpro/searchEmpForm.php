<html>
<head>
<script src="callAjax.js"></script>
<meta charset="utf-8">
<title>แจ้งการชำนะเงิน</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/callAjaxk.js"></script>
<link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    li{
      margin-top: 5px;
      font-size: 18px;
      font-family: 'Kanit', sans-serif;
    }
    h2{
      font-family: 'Kanit', sans-serif;
    }
    a{
      font-family: 'Kanit', sans-serif;
    }
    h1{
      font-family: 'Kanit', sans-serif;
    }
    abbr{
      font-family: 'Kanit', sans-serif;
    }
    input{
      font-family: 'Kanit', sans-serif;
    }
    label{
      font-family: 'Kanit', sans-serif;
    }
    .carousel-caption{
      font-family: 'Kanit', sans-serif;
    }


  </style>
</head>
<body>
  <div class="container">
      <?php include('topbar.php');?>
  </div>
<div class="container">
  <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 singlepage">
    <h1 class="title pt-2">แจ้งการชำระเงิน</h1>
    <div class="row">
      <div class="col-12 hidden-lg-down col-lg-4 col-xl-4"> </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 mt-3 pb-5" style="background-color: #fff;">
        <label for="exampleInputEmail1">กรุณาใส่เลขที่ใบสั่งซื้อ</label>
        <form>

        <input type="number" id="name" class="form-control input-lg" required placeholder="กรุณาใส่เลขที่ใบสั่งซื้อ"><br/>
        <input class="btn btn-primary" style="width: 100%;" type="button" value="เช็คสถานะการจัดส่ง" onclick="displayEmpPOST()">
        </form>

        </div>
      </div>
    </div>

</div>
</div>

<div id="showData"></div>
</body>
</html>
