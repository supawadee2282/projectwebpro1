<!DOCTYPE html>
<?php

  include("dbConfig.php");
  $order_id =$_POST['packing_no'];
  echo $_POST['packing_no'];

  $sql = "SELECT `id`, `total_price`, `created`,`tracking`, `status` FROM `orders` WHERE id=$order_id";
  $query = mysqli_query($db,$sql);
?>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div class="container">
      <h1>รายการคำสั่งซื้อ</h1>
      <table table table-striped table-bordered>
        <thead>
          <th>เลขที่ใบสั่งซื้อ</th>
          <th>วันเวลาที่สั่งซื้อ</th>
          <th>สถานะ</th>
        </thead>
        <tbody style="text-align:center">
          <?php
  					while ($row = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
  					?>
  						<tr>
  							<td><?php echo $row['id'];?></td>
  							<td><?php echo $row['created'];?></td>
  							<td><?php echo $row['status'];?></td>
  							</tr>
  				<?php
  					}
  				?>
        </tbody>
      </table>
    </div>
  </body>
</html>
