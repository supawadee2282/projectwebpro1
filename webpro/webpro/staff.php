<?php session_start();
if(!isset($_SESSION['id_member']) && $_SESSION['level']!="2")
	{
		echo "<meta http-equiv='refresh' content='0;url=connectMem.php'>";
		exit();
	}


include("topbar2.php");
include("dbConfig.php");

?>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User - admin</title>
<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="css/style.css" rel="stylesheet" type="text/css">
<!-- angularjs -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<!-- Sweet Alert -->
<script src="dist/SweetAlert.min.js"></script>
<link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
<!-- Validator -->
<link rel="stylesheet" type="text/css" href="dist/css/bootstrapValidator.min.css">
<script src="dist/js/bootstrapValidator.min.js"></script>


<style>
.inputform
{
		width: 100%;
		float: left;
}
.footBtn{width: 95%;float: left;}
.orderBtn {float: right;}

li{

	font-family: 'Kanit', sans-serif;
}
h2{
	font-family: 'Kanit', sans-serif;
}
a{
	font-family: 'Kanit', sans-serif;
}
strong{
	font-family: 'Kanit', sans-serif;
}
abbr{
	font-family: 'Kanit', sans-serif;
}
address{
	font-family: 'Kanit', sans-serif;
}
p{
	font-size: 18px;
	font-family: 'Kanit', sans-serif;
}
td{
	font-family: 'Kanit', sans-serif;
}
tr{
	font-family: 'Kanit', sans-serif;
}
button{
	font-size: 18px;
	font-family: 'Kanit', sans-serif;
}
input{
	font-size: 18px;
	font-family: 'Kanit', sans-serif;
}
textarea{
	font-size: 18px;
	font-family: 'Kanit', sans-serif;
}

</style>
</head>
<body>

  <div style="width:960px; margin:20px auto 0 auto;text-align:right;">
  <table class="data" style="margin-top:10px;" align="center" cellpadding="10" cellspacing="0">
	<div class="container ">
        <div class="inputform">
            <div class="row">
                <div class="col-md-3">
                </div>
                    <div class="col-md-5" style="background-color:#f4f4f4">
                        <h3 align="center" style="color:green">
                            <span class="glyphicon glyphicon-shopping-cart"> </span>
                                เพิ่มสินค้า </h3>
                        <form action="insertproduct.php" method="post" enctype="multipart/form-data" name="product" >
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input type="text"  name="name" class="form-control " required placeholder="ชื่อสินค้า" />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div><br><br><br>
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <textarea name="description" class="form-control css-require"  rows="3"  required placeholder="คำอธิบายสินค้า"></textarea>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div><br>
                            </div><br><br><br>
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input  type="text" name="price" class="form-control " required placeholder="ราคาสินค้า"/>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                </div>
                            </div><br><br><br>

                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input type="file" name="img" /required><span id="pic"></span>
                                </div>
                            </div><br><br>

                            <div class="form-group has-feedback">
                                <div class="col-sm-12" align="center">
                                    <button type="reset" class="btn btn-warning" id="btn" >
                                        reset </button>
                                    <button type="submit" class="btn btn-info" id="btn" >
                                        เพิ่ม </button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>

    </div>


</body>

</html>
