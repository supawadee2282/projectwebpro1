<!DOCTYPE html>
<?php
	include 'dbConfig.php';
	$query = $db->query("SELECT * FROM products");
?>


<html>
<head>
	<title>สินค้าทั้งหมด</title>
	 <!-- Bootstrap -->

    <link href="css/style.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
	<style >

		p{
			font-family: 'Kanit', sans-serif;
		}
		h1{
			font-family: 'Kanit', sans-serif;
		}
		h2{
			font-family: 'Kanit', sans-serif;
		}
		h3{
			font-family: 'Kanit', sans-serif;
		}
		h4{
			font-family: 'Kanit', sans-serif;
		}
		td{
			font-family: 'Kanit', sans-serif;
		}

	</style>
</head>
<body>
	<!-- menu top bar -->
  <div class="container">
    <?php include('topbar.php');?>
  <!-- navbar stop-->
  </div>

	<div class="container">
		<h3 align="center">สินค้าทั้งหมด</h3>
		<br/>
		<div class="table-resposive">
			<table id="product" class="table table-striped table-bordered">
				<thead>
					<tr>

						<td>รหัสสินค้า</td>
						<td>ชื่อสิค้า</td>
						<td>รายละเอียดสินค้า</td>
						<td>รูปสินค้า</td>
						<td>ราคา</td>
						<td>เพิ่มสินค้า</td>
					</tr>
				</thead>
				<?php
					while ($row = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo nl2br($row['description']);?></td>
							<td> <div class="thumbnail">
								<p><a href="showdetailproduct.php?id=<?php echo $row['id'] ?>">
									<img   src="<?php echo "imgproduct/".$row['img'];?>" class="img-responsive "width="50" height="50"	/>
									</a>
								</p>
                 				 
							</div></td>
							<td><?php echo $row['price'];?></td>
							<td><a class="btn btn-success"  href="cartAction.php?action=addToCart&id=<?php echo $row["id"]; ?>">Add to cart</a></td>
						</tr>
				<?php
					}
				?>
			</table>
		</div>

	</div>

</body>

<script >
	$(document).ready(function () {
		$('#product').DataTable();
	})
</script>
</html>
