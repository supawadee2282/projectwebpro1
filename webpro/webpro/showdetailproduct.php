<!DOCTYPE html>
<?php
	include 'dbConfig.php';
  $id =$_GET['id'];
	$query = $db->query("SELECT * FROM products where id=$id");
?>


<html>
<head>
	<title>แสดงรายละเอียดสินค้า</title>
	 <!-- Bootstrap -->

    <link href="css/style.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
	<style >

		p{
      font-size: 18px;
			font-family: 'Kanit', sans-serif;
		}
    p a{
      font-size: 25px;
			font-family: 'Kanit', sans-serif;
		}
		h1{
			font-family: 'Kanit', sans-serif;
		}
		h2{
			font-family: 'Kanit', sans-serif;
		}
		h3{
			font-family: 'Kanit', sans-serif;
		}
		h4{
			font-family: 'Kanit', sans-serif;
		}
		td{
			font-family: 'Kanit', sans-serif;
		}

	</style>
</head>
<body>
	<!-- menu top bar -->
  <div class="container">
    <?php include('topbar.php');?>
  <!-- navbar stop-->
  </div>

	<div class="container">

		<br/>
    <?php
      while ($row = mysqli_fetch_array($query)) {
      ?>
      <h1 align="center"><?php echo $row["name"]; ?></h1><br>
      <img   src="<?php echo "imgproduct/".$row['img'];?>" class="img-responsive "style=	"height: 350px;display: block;margin: auto;"/>
              <div class="caption">
                <h3>ชื่อสินค้า : <?php echo $row["name"]; ?></h3>
                <p> ราคา : <a style="color=	#0000FF"><?php echo number_format($row['price'],2);?></a> THB </p>
                <p><?php echo nl2br($row['description']);?></p>
                <a class="btn btn-success"  href="cartAction.php?action=addToCart&id=<?php echo $row["id"]; ?>">Add to cart</a>
    <?php
      }
    ?>
	</div>

</body>

<script >
	$(document).ready(function () {
		$('#product').DataTable();
	})
</script>
</html>
