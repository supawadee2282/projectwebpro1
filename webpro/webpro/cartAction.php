<?php
// initialize shopping cart class
include 'Cart.php';
$cart = new Cart;

// include database configuration file
include 'dbConfig.php';
    date_default_timezone_set('Asia/Bangkok');



if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    if($_REQUEST['action'] == 'addToCart' && !empty($_REQUEST['id']))
    {
        $productID = $_REQUEST['id'];
        // get product details
        $sql1 = "SELECT * FROM products WHERE id = ".$productID;
        $query = mysqli_query($db,$sql1);
        $row = mysqli_fetch_assoc($query);
        $itemData = array(
            'id' => $row['id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'qty' => 1
        );

        $insertItem = $cart->insert($itemData);
        $redirectLoc = $insertItem?'viewCart.php':'index.php';
        header("Location: ".$redirectLoc);
    }elseif($_REQUEST['action'] == 'updateCartItem' && !empty($_REQUEST['id']))
    {
        $itemData = array(
            'rowid' => $_REQUEST['id'],
            'qty' => $_REQUEST['qty']
        );
        $updateItem = $cart->update($itemData);
        echo $updateItem?'ok':'err';die;
    }elseif($_REQUEST['action'] == 'removeCartItem' && !empty($_REQUEST['id']))
    {
        $deleteItem = $cart->remove($_REQUEST['id']);
        header("Location: viewCart.php");
    }elseif($_REQUEST['action'] == 'placeOrder' && $cart->total_items() > 0  )
    {


        // insert order details into database
        $totalorder1    =$cart->total();
        $order_date     =date("Y-m-d H:i:s");
        $name           =$_SESSION['name'];
        $address        =$_SESSION['address'];
        $phone          =$_SESSION['phone'];
        $email          =$_SESSION['email'];

        $insertOrder = $db->query("INSERT INTO orders (total_price, created, modified,name,email,phone,address) VALUES ('".$cart->total()."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."','$name','$email','$phone','$address')");


        if($insertOrder)
        {
            $orderID = $db->insert_id;
            $sql = '';
            // get cart items
            $cartItems = $cart->contents();
            foreach($cartItems as $item)
            {
                $sql .= "INSERT INTO order_items (order_id, product_id, quantity) VALUES ('".$orderID."', '".$item['id']."', '".$item['qty']."');";
            }
            // insert order items into database
            $insertOrderItems = $db->multi_query($sql);

           /*$queryOrder=$db->query("SELECT MAX(id) AS id FROM orders WHERE phone='$phone'");
           $row = mysqli_fetch_array($queryOrder);
           $order_id = $row['id'];
           $_SESSION['order_id'] = $order_id;*/


            if($insertOrderItems)
            {
                $cart->destroy();
                header("Location: orderSuccess.php?id=$orderID");
            }else
            {
                header("Location: checkout.php");
            }
        }else
        {
            header("Location: checkout.php");
        }
    }else
    {
        header("Location: index.php");
    }
}
else
{
    header("Location: index.php");
}
?>
