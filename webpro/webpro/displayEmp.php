<?php
 include("dbconfig.php");
 $name = $_POST['n'];
?>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!-- import font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <style >

        p{
          font-family: 'Kanit', sans-serif;
        }
        h1{
          font-family: 'Kanit', sans-serif;
        }
        th{
          text-align: center;
          font-family: 'Kanit', sans-serif;
        }
        h3{
          font-family: 'Kanit', sans-serif;
        }
        h4{
          font-family: 'Kanit', sans-serif;
        }
        td{
          font-family: 'Kanit', sans-serif;
        }

      </style>


  </head>
  <body>
    <div class="container">
      <div class="table-resposive">
        <h1>รายการคำสั่งซื้อ</h1>
        <table class="table table-striped table-bordered table-r">
          <thead >
            <th>เลขที่ใบสั่งซื้อ</th>
            <th>วันเวลาที่สั่งซื้อ</th>
            <th>สถานะ</th>
            <th>tracking number</th>
            <th>รายการสั่งซื้อ</th>
            <th>แนนสลิป</th>
            <th>สลิป</th>
          </thead>
          <tbody style="text-align:center">


            <?php
       			//get rows query
       			$query = $db->query("SELECT `id`,`created`,`tracking`, `status` FROM `orders` WHERE id=$name");
       			if($query->num_rows > 0){
       					while($row = $query->fetch_assoc()){
       			?>
            <tr>
              <td><?php echo $row['id'];?></td>
              <td><?php echo $row['created'];?></td>
              <td><?php
                    if ($row['status']==1) {
                      ?><p class="text-warning"><?php echo "รอการยื่นยัน"; ?></p>

                  <?php  }
                    if ($row['status']==0) {
                      ?> <p class="text-success"><?php echo "กำลังจัดส่งสินค้า";?></p>
                  <?php  }

              ?></td>
              <td><?php echo $row['tracking'];?></td>
              <td><a href="showlistbuy.php?id=<?php echo $row['id'];?>" class="btn btn-info" >VIEW</a> </td>
              <td><a href="add_pic_confirm.php?id=<?php echo $row['id'];?>" class="btn btn-success"><i class="glyphicon glyphicon-picture"></i></a> </td>
              <td><a href="viewslip.php?id=<?php echo $row['id'];?>" class="btn btn-info" >VIEW</a> </td>
            </tr>
       			<?php
       			}
       			}else{
       			?>
       			<h4 class="text-danger">ไม่พยหมายเลขสั่งซื้อนี้...... #<?php echo $_POST['n'];?></h4>
       			<?php
       			}
       			?>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
