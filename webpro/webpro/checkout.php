<?php
// include database configuration file
include 'dbConfig.php';

// initializ shopping cart class
include 'Cart.php';
$cart = new Cart;

// redirect to home if cart is empty
if($cart->total_items() <= 0){
    header("Location: index.php");
}

// set customer ID in session
//$_SESSION['sessCustomerID'] = 1;

// get customer details by session customer ID
//$query = $db->query("SELECT * FROM customers WHERE id = ".$_SESSION['sessCustomerID']);
//$custRow = $query->fetch_assoc();
?>
<!DOCTYPE html>
<html >
<head>
    <title>Checkout</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!-- angularjs -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

    <!-- Sweet Alert -->
    <script src="dist/SweetAlert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">
    <!-- Validator -->
    <link rel="stylesheet" type="text/css" href="dist/css/bootstrapValidator.min.css">
    <script src="dist/js/bootstrapValidator.min.js"></script>


    <style>
    .table{width: 100%;float: left;}
    .shipAddr
    {
        width: 100%;
        float: left;
    }
    .footBtn{width: 95%;float: left;}
    .orderBtn {float: right;}

    li{

      font-family: 'Kanit', sans-serif;
    }
    h2{
      font-family: 'Kanit', sans-serif;
    }
    a{
      font-family: 'Kanit', sans-serif;
    }
    strong{
      font-family: 'Kanit', sans-serif;
    }
    abbr{
      font-family: 'Kanit', sans-serif;
    }
    address{
      font-family: 'Kanit', sans-serif;
    }
    p{
      font-size: 18px;
      font-family: 'Kanit', sans-serif;
    }
    td{
      font-family: 'Kanit', sans-serif;
    }
    tr{
      font-family: 'Kanit', sans-serif;
    }
    button{
      font-size: 18px;
      font-family: 'Kanit', sans-serif;
    }
    input{
      font-size: 18px;
      font-family: 'Kanit', sans-serif;
    }
    textarea{
      font-size: 18px;
      font-family: 'Kanit', sans-serif;
    }

    </style>
    <!--.orderBtn {float: right;} -->
</head>
<body>
    <div class="container">
        <?php include('topbar.php');?>
    </div>
    <div class="container">
        <h1>Order</h1>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
        <tbody>
        <?php
        if($cart->total_items() > 0){
            //get cart items from session
            $cartItems = $cart->contents();
            foreach($cartItems as $item){
        ?>
        <tr>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo number_format($item["price"],2).' THB'; ?></td>
            <td><?php echo $item["qty"]; ?></td>
            <td><?php echo number_format($item["subtotal"],2).' THB'; ?></td>
        </tr>
        <?php } }else{ ?>
        <tr><td colspan="4"><p>No items in your cart......</p></td>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <?php if($cart->total_items() > 0){ ?>
            <td class="text-center text-danger"><strong>Total <?php echo number_format($cart->total(),2).' THB'; ?></strong></td>
            <?php } ?>
        </tr>
    </tfoot>
    <hr>
    </table>


    <div class="container ">

        <div class="shipAddr">
            <div class="row">
                <div class="col-md-3">
                </div>
                    <div class="col-md-5" style="background-color:#f4f4f4">
                        <h3 align="center" style="color:green">
                            <span class="glyphicon glyphicon-shopping-cart"> </span>
                                Confirm cart </h3>
                        <form action="checkout.php" method="POST" id="Order-form" class="form-horizontal" accept-charset="UTF-8">
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input type="text"  name="name" class="form-control css-require" required placeholder="ชื่อ-สกุล"  />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <textarea name="address" class="form-control css-require"  rows="3"  required placeholder="ที่อยู่ในการส่งสินค้า"></textarea>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input  type='tel'  name="phone" class="form-control " required placeholder="เบอร์โทรศัพท์" pattern="^\d{10}$" />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-sm-12">
                                    <input type="email"  name="email" class="form-control " required placeholder="อีเมล์" />
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-sm-12" align="center">
                                    <button type="reset" class="btn btn-warning" id="btn" >
                                        reset </button>
                                    <button type="submit" class="btn btn-info" id="btn" >
                                        ยืนยันที่อยู่ </button>

                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>

    </div>
<?php
    if (!empty($_POST['name'])) {
        $_SESSION['name'] = $_POST['name'];
        $_SESSION['address'] = $_POST['address'];
        $_SESSION['phone'] = $_POST['phone'];
        $_SESSION['email'] = $_POST['email'];


    }
?>

<div class="shipAddr">
        <h3 class="text-primary">Shipping Details</h3>
        <p>ชื่อ-สกุล :
            <?php

                if (!empty($_POST['name'])) {
                    echo $_POST['name'];
                }
            ?>
        </p>
        <p>อีเมล์ :
            <?php

                if (!empty($_POST['email'])) {
                    echo $_POST['email'];
                }
            ?>
        </p>
        <p>เบอร์โทรศัพท์ :
            <?php

                if (!empty($_POST['phone'])) {
                    echo $_POST['phone'];
                }
            ?>
        </p>
        <p>ที่อยู่ในการส่งสินค้า :
            <?php

                if (!empty($_POST['address'])) {
                    echo $_POST['address'];
                }
            ?>
        </p>

</div>
    <div class="footBtn" style="margin-top:1px;">
        <a href="index.php" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Shopping</a>
        <?php
            if (!empty($_POST['name']) && !empty($_POST['email'])&&!empty($_POST['phone'])&&!empty($_POST['address']) )
        {
        ?>
        <a href="cartAction.php?action=placeOrder"  onclick="return confirm('Are you sure?')" class="btn btn-success orderBtn">Place Order <i class="glyphicon glyphicon-menu-right"></i></a>
        <?php } ?>
    </div>

</div>
</body>
<script type="text/javascript">
      $(document).ready(function() {
        var validator = $("#Order-form").bootstrapValidator({
          fields :{
            email :{
              message : "Email address is required",
              validators :{
                notEmpty :{
                  message : "Please provide an email address"
                }
              }
            }
          }
        });
      });

$(function(){

     var obj_check=$(".css-require");
     $("#myform1").on("submit",function(){
         obj_check.each(function(i,k){
             var status_check=0;
             if(obj_check.eq(i).find(":radio").length>0 || obj_check.eq(i).find(":checkbox").length>0){
                 status_check=(obj_check.eq(i).find(":checked").length==0)?0:1;
             }else{
                 status_check=($.trim(obj_check.eq(i).val())=="")?0:1;
             }
             formCheckStatus($(this),status_check);
         });
         if($(this).find(".has-error").length>0){
              return false;
         }
     });

     obj_check.on("change",function(){
         var status_check=0;
         if($(this).find(":radio").length>0 || $(this).find(":checkbox").length>0){
             status_check=($(this).find(":checked").length==0)?0:1;
         }else{
             status_check=($.trim($(this).val())=="")?0:1;
         }
         formCheckStatus($(this),status_check);
     });

     var formCheckStatus = function(obj,status){
         if(status==1){
             obj.parent(".form-group").removeClass("has-error").addClass("has-success");
             obj.next(".glyphicon").removeClass("glyphicon-warning-sign").addClass("glyphicon-ok");
         }else{
             obj.parent(".form-group").removeClass("has-success").addClass("has-error");
             obj.next(".glyphicon").removeClass("glyphicon-ok").addClass("glyphicon-warning-sign");
         }
     }

 });
</script>

</html>
