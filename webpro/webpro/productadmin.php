<!DOCTYPE html>
<?php
	include 'dbConfig.php';
	session_start();
	if(!isset($_SESSION['id_member']) && $_SESSION['level']!="2")
		{
			echo "<meta http-equiv='refresh' content='0;url=connectMem.php'>";
			exit();
		}

	$query = $db->query("SELECT * FROM products");
?>


<html>
<head>


	<title>สินค้าทั้งหมด</title>
	 <!-- Bootstrap -->

    <link href="css/style.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/>

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
	<?php include("topbar3.php"); ?>


	<div class="container">

		<h3 align="center">สินค้าทั้งหมด</h3>
		<br>

		<br/>
		<div class="table-resposive">

			<table id="product" class="table table-striped table-bordered">
				<thead>

					<tr align="center">

						<td>รหัสสินค้า</td>
						<td>ชื่อสิค้า</td>
						<td>รายละเอียดสินค้า</td>
						<td>รูปสินค้า</td>
						<td>ราคา</td>
						<td width="100px;" >แก้ไข</td>
					</tr>
				</thead>
				<?php
					while ($row = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $row['description'];?></td>
							<td> <div class="thumbnail">
                 				 <img src="<?php echo "imgproduct/".$row['img'];?>"  width="50" height="50" /><br>
							</div></td>
							<td><?php echo $row['price'];?></td>
							<td><a class="btn btn-success"  href="editProduct.php?id=<?php echo ($row['id'])?>">แก้ไข</a>
								<a class="btn btn-success"  href="deleteProduct.php?id=<?php echo ($row['id'])?>">ลบ</a>
							</td>
						</tr>
				<?php
					}
				?>
			</table>
		</div>

	</div>

</body>

<script >
	$(document).ready(function () {
		$('#product').DataTable();
	})
</script>
</html>
