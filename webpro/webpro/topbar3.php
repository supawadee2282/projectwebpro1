<!DOCTYPE html>
<html>
  <head>

    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


      <style>
        li{
          margin-top: 5px;
          font-size: 18px;
          font-family: 'Kanit', sans-serif;
        }
        h2{
          font-family: 'Kanit', sans-serif;
        }
        a{
          font-family: 'Kanit', sans-serif;
        }
        strong{
          font-family: 'Kanit', sans-serif;
        }
        abbr{
          font-family: 'Kanit', sans-serif;
        }
        address{
          font-family: 'Kanit', sans-serif;
        }
        p{
          font-family: 'Kanit', sans-serif;
        }
        .carousel-caption{
          font-family: 'Kanit', sans-serif;
        }
        li{
          margin-top: 5px;
          font-size: 18px;
          font-family: 'Kanit', sans-serif;
        }


      </style>

  </head>
  <body>
    <!-- navbar start-->
  ﻿<nav class="navbar-fixed-top  container"style="background-color: #FFFFFF; ">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar  navbar-header ">

      <button class="navbar-toggle " data-toggle="collapse" data-target="#navHeader" style="position:relative;">
        <span class="sr-only">(current)</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="navHeader" style="position:relative;">
      <ul class="nav navbar-nav">
        <li ><a href="useradmin.php">หน้าหลัก
          <span class="sr-only">(current)</span></a>
        </li>
        <li id="navHeader">
          <a class="active" href="productadmin.php">สินค้าทั้งหมด</a>
        </li>
        <li>
          <a href="orderadmin.php ?>">การสั่งซื้อ</a>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right hidden-sm">



         <li class="nav-item">
          <?php
            if( !isset($_SESSION['id_member']) ){
              echo "<a href=connectMem.php>เข้าสู่ระบบ</a>";
            }
            else{?>
             <!-- // echo "<a href=logout.php>". $_SESSION['m_username'] ." </a>"; -->
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
            <?php
                echo $_SESSION['m_username'];
            ?>
            <span class="caret"></span>
            </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="logout.php">ออกจากระบบ</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtobuy.html -->
                </li>
                <li>
                  <a href="fromupdateregister.php">แก้ไขข้อมูลส่วนตัว</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtobuy.html -->
                </li>
              </ul>
            </li>
              <?php
              }

              ?>
        </li>

      </ul>

    </div>
    <!-- /.navbar-collapse -->
  </nav>
  <br>
  <br>
  <br>
  </body>
</html>
