<!DOCTYPE html>

<?php
// include database configuration file
include 'dbConfig.php';
?>
<html >
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ethereum.com</title>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <!-- import font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      li{
        margin-top: 5px;
        font-size: 18px;
        font-family: 'Kanit', sans-serif;
      }
      h2{
        font-family: 'Kanit', sans-serif;
      }
      a{
        font-family: 'Kanit', sans-serif;
      }
      strong{
        font-family: 'Kanit', sans-serif;
      }
      abbr{
        font-family: 'Kanit', sans-serif;
      }
      address{
        font-family: 'Kanit', sans-serif;
      }
      p{
        font-family: 'Kanit', sans-serif;
      }
      .carousel-caption{
        font-family: 'Kanit', sans-serif;
      }


    </style>


</head>
<body >
	<!-- menu top bar -->
  <div class="container">
    <?php include('topbar.php');?>
  <!-- navbar stop-->
  </div>
<div class="container">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img/head2.jpg" alt="thumb" style="width:100%;">

      </div>
      <div class="item">
        <img src="img/Screenshot_2.jpg" alt="thumb" style="width:100%;">

      </div>
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<hr>
<h2 class="text-center">RECOMMENDED PRODUCTS</h2>
<hr>
<?php include('showproduct.php');?>
<hr>


<footer class="text-center">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p>Copyright © CipherLab_3CS. All rights reserved.</p>
      </div>
    </div>
  </div>
</footer>
</body>
</html>
