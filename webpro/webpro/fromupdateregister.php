<?php session_start();

include("dbConfig.php");

?>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User - Member</title>
<link href="css/design.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript"></script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;

}

.button1 {
    background-color: green;
    color: black;
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}
</style>


<script>
function minInput(name,min){
    if($(name).val().length < min){
       alert('ต้องป้อนอย่างน้อย'+min+' ตัว!!');
       return false;
    }else{
        return true;
    }
}
</script>

<script>
function check_form(name_form){
	var format_mail=/^([a-zA-Z]{1,})+([a-zA-Z0-9\_\-\.]{1,})+@([a-zA-Z0-9\-\_]{1,})+([.]{1,1})+([a-zA-Z0-9\-\_\.]{1,})$/;
	if(!(format_mail.test(name_form.email.value)))
	{
		alert("รูปแบบอีเมล์ไม่ถูกต้อง");
		name_form.email.focus();
		return false;
	}
}
</script>


</head>



<div style="clear:both;"></div>
<body>

  <div style="width:960px; margin:20px auto 0 auto;text-align:right;">
  <table class="data" style="margin-top:10px;" align="center" cellpadding="10" cellspacing="0">
  <tr>
    <th class="header" colspan="2">แก้ไขข้อมูลสมาชิก</th>
  </tr>

  <tr>
    <td colspan="2">
<form action="updateregister.php" method="post" >

<table width="650px" border="0" align="center" cellpadding="5" cellspacing="0">
<tr >
    <td>ประเภทผู้ใช้</td>
    <td><input type="radio" name="level"  id="radio" value="3" checked="checked" <?php isset($POST['u_type'])?  "checked" :''; ?> /> สมาชิก
    </td>
</tr>
<tr>
  
 	<td> ชื่อ : </td>
    <td><input type="text" name="name" onKeyUp="if(!(isNaN(this.value))) { alert('กรุณากรอกอักษร'); this.value='';}" /required></td>
</tr>
<tr>
	<td> E-mail : </td>
    <td><input type="email"  name="email" /required></td>
</tr>

<tr>
	<td> เบอร์โทรศัพท์ : </td>
    <td><input type="text" name="Tel" maxlength="10" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}" /required></td>
</tr>
<tr> <td> ที่อยู่ : </td>
	<td><textarea  name="addr" style="width:auto;"/required></textarea></td>
</tr>
<tr> <td> Password : </td>
    <td><input type="password" name="password" /required></td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="ยืนยัน" /></td>
</tr>
</table>
</td>
</tr>
</table>
</form>

</body>
</html>
