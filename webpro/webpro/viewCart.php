<?php
// initializ shopping cart class
include 'Cart.php';
$cart = new Cart;
?>
<!DOCTYPE html>
<html >
<head>
    <title>View Cart</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css">

    <!-- Sweet Alert -->
    <script src="dist/SweetAlert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="dist/sweetalert.css">

    <style>
    input[type="number"]{width: 20%;}

      li{

        font-family: 'Kanit', sans-serif;
      }
      h2{
        font-family: 'Kanit', sans-serif;
      }
      a{
        font-family: 'Kanit', sans-serif;
      }
      strong{
        font-family: 'Kanit', sans-serif;
      }
      abbr{
        font-family: 'Kanit', sans-serif;
      }
      address{
        font-family: 'Kanit', sans-serif;
      }
      p{
        font-family: 'Kanit', sans-serif;
      }
      td{
        font-family: 'Kanit', sans-serif;
      }
      tr{
        font-family: 'Kanit', sans-serif;
      }


    </style>

    <script>
    function updateCartItem(obj,id){
        $.get("cartAction.php", {action:"updateCartItem", id:id, qty:obj.value}, function(data){
            if(data == 'ok'){
                location.reload();
            }else{
                alert('Cart update failed, please try again.');
            }
        });
    }
    </script>

</head>
</head>
<body>
    <div class="container">
        <?php include('topbar.php');?>
    </div>

<div class="container ">

        <h1>Shopping Cart</h1>
    <table class="table table-striped">
    <thead>
        <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if($cart->total_items() > 0){
            //get cart items from session
            $cartItems = $cart->contents();
            foreach($cartItems as $item){
        ?>
        <tr>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo number_format($item["price"],2).' THB';?></td>

            <td><input type="number" class="form-control text-center" value="<?php echo $item["qty"]; ?>" onchange="updateCartItem(this, '<?php echo $item["rowid"]; ?>')"></td>
            <td><?php echo number_format($item["subtotal"],2).' THB'; ?></td>
            <td>
                <a href="cartAction.php?action=removeCartItem&id=<?php echo $item["rowid"]; ?>" class="btn btn-danger"  onclick="return confirm('Are you sure?')"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>


        <?php } }else{ ?>
        <tr><td colspan="5"><p>Your cart is empty.....</p></td>
        <?php } ?>
    </tbody>

    <tfoot>

        <tr>
            <td><a href="index.php" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Shopping</a></td>
            <td colspan="2"></td>
            <?php if($cart->total_items() > 0){ ?>
            <td class="text-center"><strong>Total <?php echo number_format($cart->total(),2).' THB'; ?></strong></td>
            <td><a href="checkout.php" class="btn btn-success btn-block">Checkout <i class="glyphicon glyphicon-menu-right"></i></a></td>
            <?php } ?>
        </tr>
    </tfoot>
    </table>

</div>
</body>
</html>
