<?php
$host= 'localhost';
$user= 'root';
$password= '';
$db='dbshop';
//connect to database
$conn = mysqli_connect($host,$user,$password,$db);
//check connection
if (mysqli_connect_errno()) {
    echo "Connect failed: ".mysqli_connect_error();
    exit();
}
//set charset for thai
mysqli_set_charset($conn, "utf8");

mysqli_query($conn,"SET NAMES UTF8");
?>
