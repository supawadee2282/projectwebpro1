<!DOCTYPE html>

<?php
// include database configuration file
include 'dbConfig.php';
$id = $_GET['id'];



?>
<html >
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ethereum.com</title>
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <!-- import font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      li{
        margin-top: 5px;
        font-size: 18px;
        font-family: 'Kanit', sans-serif;
      }
      h1{
        font-family: 'Kanit', sans-serif;
      }
      a{
        font-family: 'Kanit', sans-serif;
      }
      strong{
        font-family: 'Kanit', sans-serif;
      }
      abbr{
        font-family: 'Kanit', sans-serif;
      }
      address{
        font-family: 'Kanit', sans-serif;
      }
      p{
        font-family: 'Kanit', sans-serif;
      }
      .carousel-caption{
        font-family: 'Kanit', sans-serif;
      }
      th{
          text-align: center;
          font-family: 'Kanit', sans-serif;
      }
      tr{
          text-align: center;
          font-family: 'Kanit', sans-serif;
      }
      @media print {
        #btnprint{
          display:  none;
        }
        #btnprint2{
          display:  none;
        }
      }

    </style>
    <script>
    function goBack() {
    window.history.back();
}
</script>


</head>
<body >
  <div class="container" style="margin-top: 5%;">
    <button class="btn btn-warning" onclick="goBack()" id="btnprint">Go Back</button>
  </div>


  <div class="container" >
    <div class="table-resposive" >
      <br>
      <h1>รายการคำสั่งซื้อหมายเลข :<?php echo $id ;?></h1>
      <button type="button" name="button" id="btnprint" class="btn btn btn-info" onclick="window.print();">พิมพ์รายการ</button><br><br>
      <table class="table table-striped table-bordered">
        <thead >
          <th>No.</th>
          <th>รายการ</th>
          <th>จำนวน</th>

        </thead>
        <tbody style="text-align:center">
          <?php
          //get rows query
          $query = $db->query("SELECT products.id,products.name,order_items.quantity,orders.total_price FROM `order_items`,products,orders WHERE order_items.order_id =$id  AND order_items.product_id = products.id AND order_items.order_id = orders.id");
          if($query->num_rows > 0){
            $i=0;
              while($row = $query->fetch_assoc()){
                $i=$i+1;
                $total =$row['total_price'];
          ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $row['name'];?></td>
            <td><?php echo $row['quantity'];?></td>
          </tr>

          <?php
          }
          }else{
          ?>

          <h4 class="text-danger">Not found...... #<?php echo $_POST['n'];?></h4>
          <?php
          }
          ?>
        </tbody>
        <tr>
          <td></td>
          <td></td>
          <td><?php echo $total;?> บาท </td>
        </tr>
      </table>

    </div>
  </div>

</body>
</html>
