<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>

    <title></title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <style>
      li{
        margin-top: 5px;
        font-size: 18px;
        font-family: 'Kanit', sans-serif;
      }
    </style>
  </head>
  <body>
    <!-- navbar start-->
  ﻿<nav class="navbar-fixed-top  container"style="background-color: #FFFFFF; ">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar  navbar-header ">
      
      <button class="navbar-toggle " data-toggle="collapse" data-target="#navHeader" style="position:relative;">
        <span class="sr-only">(current)</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="navHeader" style="position:relative;">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">HOME
          <span class="sr-only">(current)</span></a>
        </li>
        <li id="navHeader">
          <a href="showallproduct.php">สินค้าทั้งหมด</a>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right hidden-sm">

        <li>
          <a class=""href="viewCart.php">
            <span class="glyphicon glyphicon-shopping-cart"></span> ตะกร้า
          </a>
        </li>
        <li>
          <a href="searchEmpForm.php">แจ้งการชำระเงิน</a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
            วิธีสั่งซื้อ/ชำระเงิน
            <span class="caret"></span>
          </a>
        <ul class="dropdown-menu">
          <li>
            <a href="howtoorder.php">วิธีการสั่งซื้อ</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtobuy.html -->
          </li>
          <li>
            <a href="howtopay.php">วิธีการชำระเงิน</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtoPayment.html -->
          </li>
        </ul>
        </li>
        <li class="nav-item">
          
          <a href="register.php">สมัครสมาชิก</a>
        </li>
         <li class="nav-item">
          <?php
            if( !isset($_SESSION['id_member']) ){
              echo "<a href=connectMem.php>เข้าสู่ระบบ</a>";
            }
            else{?>
             <!-- // echo "<a href=logout.php>". $_SESSION['m_username'] ." </a>"; -->
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
            <?php
                echo $_SESSION['m_username'];
            ?>
            <span class="caret"></span>
            </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="logout.php">ออกจากระบบ</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtobuy.html -->
                </li>
                <li>
                  <a href="fromupdateregister.php">แก้ไขข้อมูลส่วนตัว</a> <!-- link ไปหน้าวิธีการสั่งซื้อ เช่น howtobuy.html -->
                </li>
              </ul>
            </li>
              <?php
              }

              ?>
        </li>

      </ul>

    </div>
    <!-- /.navbar-collapse -->
  </nav>
  <br>
  <br>
  <br>
  </body>
</html>