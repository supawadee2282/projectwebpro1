
<!DOCTYPE html>

<?php
// include database configuration file
include 'dbConfig.php';

$dsn = 'mysql:host=localhost;dbname=dbshop';
$user='root';
$conn = new PDO($dsn,$user,"");

//start cut page
$page = isset($_GET['page'])?(int)$_GET['page']:1;
$per_page=isset($_GET['per_page'])?(int)$_GET['per_page']:8;
$start = ($page>1)?($page*$per_page)-$per_page:0;

//echo "page :".$page." per_page :".$per_page.": Start :".$start ;

$product =$conn->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM products limit {$start},{$per_page}");
$product->execute();

$productname=$product->fetchALL(PDO::FETCH_ASSOC);
$total=$conn->query("SELECT FOUND_ROWS() as total")->fetch()['total'];
//echo " Total :".$total;

//ceil flost to int
$pagecount = ceil($total/$per_page);

?>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>

		<style >
			.selected{
				background-color: #4CAF50;
				font-weight: bold;
			}
			#nonselected{
				color: black;
				float: left;
				padding: 8px 16px;
				text-decoration: none;
				transition: background-color .3s;

			}
			#nonselected:hover{
				background-color:#ddd;
			}
			p{
			  font-family: 'Kanit', sans-serif;

			}
			h4{
				font-size: 15px;
			  font-family: 'Kanit', sans-serif;

			}

		</style>
<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="container" >
			<?php
			//get rows query
			$query = $db->query("SELECT SQL_CALC_FOUND_ROWS * FROM products limit {$start},{$per_page}");
			if($query->num_rows > 0){
					while($row = $query->fetch_assoc()){
			?>
			<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="thumbnail" style="height: 450px;">
							<div >
				          <p><a href="showdetailproduct.php?id=<?php echo $row['id'] ?>">
										<img   src="<?php echo "imgproduct/".$row['img'];?>" class="img-responsive "
										style=	"height: 280px;
														 display: block;
														 margin: auto;"
										/>
										</a>
									</p>
									<br>
				        	<div class="caption">
								<h4>ชื่อสินค้า : <?php echo $row["name"]; ?></h4>
								 <p> ราคา : <?php echo number_format($row['price'],2).' THB';?></p>
						  	 </div>
							<center><a class="btn btn-success" href="cartAction.php?action=addToCart&id=<?php echo $row["id"]; ?>">Add to cart</a></center>
						</div>
					</div>
			</div>
			<?php
			}
			}else{
			?>
			<p>Product(s) not found.....</p>
			<?php
			}
			?>

		</div>
		<div class="container ">

				<?php for ($i=1; $i<=$pagecount ; $i++) :?>
				<a href="?page=<?php echo $i;?>&per_page=<?php echo $per_page;?>"<?php if($page===$i){echo" class=selected";}?><?php {echo" id=nonselected";}?> ><?php echo $i;?></a>
				<?php endfor;?>

		</div>
	</body>
</html>
