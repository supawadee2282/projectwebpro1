
<?php session_start();
if(!isset($_SESSION['id_member']) && $_SESSION['level']!="1")
	{
		echo "<meta http-equiv='refresh' content='0;url=connectMem.php'>";
		exit();
	}

include("dbConfig.php");


?>
<head>

  <link href="css/style.css" rel="stylesheet" type="text/css"> 
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"/> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script> 

<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User - admin</title>
<link href="css/design.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript"></script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;

}

.button1 {
    background-color: green;
    color: black;
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
}
</style>


<script>
function minInput(name,min){
    if($(name).val().length < min){
       alert('ต้องป้อนอย่างน้อย'+min+' ตัว!!');
       return false;
    }else{
        return true;
    }
}
</script>

<script>
function check_form(name_form){
	var format_mail=/^([a-zA-Z]{1,})+([a-zA-Z0-9\_\-\.]{1,})+@([a-zA-Z0-9\-\_]{1,})+([.]{1,1})+([a-zA-Z0-9\-\_\.]{1,})$/;
	if(!(format_mail.test(name_form.email.value)))
	{
		alert("รูปแบบอีเมล์ไม่ถูกต้อง");
		name_form.email.focus();
		return false;
	}
}
</script>


</head>



<div style="clear:both;"></div>
<body>
   <?php include("topbar3.php"); ?>

  <!-- <div style="width:960px; margin:20px auto 0 auto;text-align:right;"> -->


  <table class="data" style="margin-top:22px;" align="center" cellpadding="10" cellspacing="0">
  <tr>
    <th class="header" colspan="2">เพิ่มพนักงาน</th>
  </tr>

  <tr>
    <td colspan="2">
<form action="adminInsert.php" method="post" >

<table width="650px" border="0" align="center" cellpadding="5" cellspacing="0">
<tr >
    <td>ประเภทผู้ใช้</td>
    <td><input type="radio" name="level"  id="radio" value="2" checked="checked" <?php isset($POST['u_type'])?  "checked" :''; ?> /> พนักงาน
    </td>
</tr>
<tr>
  <br>
 	<td> ชื่อพนักงาน : </td>
    <td><input type="<text" name="name" onKeyUp="if(!(isNaN(this.value))) { alert('กรุณากรอกอักษร'); this.value='';}" /required></td>
</tr>
<tr>
	<td> E-mail : </td>
    <td><input type="email"  name="email" /required></td>
</tr>

<tr>
	<td> เบอร์โทรศัพท์ : </td>
    <td><input type="text" name="Tel" maxlength="10" onKeyUp="if(isNaN(this.value)){ alert('กรุณากรอกตัวเลข'); this.value='';}" /required></td>
</tr>
<tr> <td> ที่อยู่ : </td>
	<td><textarea  name="addr" style="width:auto;"/required></textarea></td>
</tr>

<tr> <td> Username : </td>
    <td><input type="text" name="m_username" /required></td>
</tr>

<tr> <td> Password : </td>
    <td><input type="password" name="password" /required></td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="เพิ่ม" /></td>
</tr>
</table>
</td>
</tr>
</table>
</form>

<?php
      include("connect1.php");
        $result = $db->prepare("select * from member where level ='1' order by id_member");
        $result->execute();
        $row = $result->rowcount();

      ?>
    <div style="text-align:center;">
        จำนวนแอดมินทั้งหมด : <font color="green" style="font:bold 20px 'Aleo';"> [<?php echo $row;?>] คน </font> <!--นับจำนวนแอดมิน เลเวล1-->

<table class="list" align="center" cellpadding="15" cellspacing="0" style="width: 1000px ;" >
  <tr  >
   <th class="header">รหัส</th>
    <th width="80px;" class="header">ชื่อเข้าใช้</th>
    <th width="150px;" class="header">ชื่อผู้ใช้</th>
    <th class="header">E-mail</th>
    <th class="header">ที่อยู่</th>
    <th class="header">เบอร์โทร</th>
    <th class="header">ประเภท</th>


  </tr>

<?php
  include("connect1.php");
  include("dbConfig.php");
  $sql= "select * from member where level ='1' order by id_member";
  $result=mysqli_query($db,$sql);
  //for($i=0; $data=$sql->fetch(); $i++ );{
    while($data=mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
    ?>
      <tr >

            <td> <?php echo $data['id_member'];  ?> </td>
            <td> <?php echo $data['m_username'];  ?> </td>
            <td> <?php echo $data['name'];  ?> </td>
            <td> <?php echo $data['email'];  ?> </td>
            <td> <?php echo $data['addr'];  ?> </td>
            <td> <?php echo $data['Tel'];  ?> </td>
            <td> <?php echo $data['level'];  ?> </td>

  </tr>



          <?php }
          ?>
 </table>

<?php
			include("connect1.php");
				$result = $db->prepare("select * from member where level ='3' order by id_member");
				$result->execute();
				$row = $result->rowcount();

			?>
		<div style="text-align:center;">
        จำนวนลูกค้าทั้งหมด : <font color="green" style="font:bold 20px 'Aleo';"> [<?php echo $row;?>] คน </font> <!--นับจำนวนแอดมิน เลเวล1-->

</div>
<table class="list" align="center" cellpadding="15" cellspacing="0" style="width: 1000px ;" >
  <tr  >
   <th class="header">รหัส</th>
    <th width="80px;" class="header">ชื่อเข้าใช้</th>
    <th width="150px;" class="header">ชื่อผู้ใช้</th>
    <th class="header">E-mail</th>
    <th class="header">ที่อยู่</th>
    <th class="header">เบอร์โทร</th>
    <th class="header">ประเภท</th>


  </tr>

<?php
	include("connect1.php");
  include("dbConfig.php");
	$sql= "select * from member where level ='3' order by id_member";
	$result=mysqli_query($db,$sql);
	//for($i=0; $data=$sql->fetch(); $i++ );{
		while($data=mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
		?>
			<tr >

            <td> <?php echo $data['id_member'];  ?> </td>
            <td> <?php echo $data['m_username'];  ?> </td>
            <td> <?php echo $data['name'];  ?> </td>
            <td> <?php echo $data['email'];  ?> </td>
            <td> <?php echo $data['addr'];  ?> </td>
            <td> <?php echo $data['Tel'];  ?> </td>
            <td> <?php echo $data['level'];  ?> </td>

  </tr>



          <?php }
          ?>
 </table>



<br>
<br>
<?php
			 include("connect1.php");
        $result = $db->prepare("select * from member where level ='2' order by id_member");
        $result->execute();
        $row = $result->rowcount();

      ?>
    <div style="text-align:center;">
        จำนวนพนักงานทั้งหมด : <font color="green" style="font:bold 20px 'Aleo';">[<?php echo $row;?>] คน </font>


</div>
<table class="list" align="center" cellpadding="15" cellspacing="0" style="width: 1000px ;">
  <tr>
    <th class="header">รหัส</th>
    <th width="80px;"class="header">ชื่อเข้าใช้</th>
    <th width="150px;" class="header">ชื่อผู้ใช้</th>
    <th class="header">E-mail</th>
    <th class="header">ที่อยู่</th>
    <th class="header">เบอร์โทร</th>
    <th class="header">ประเภท</th>

     <th class="header">จัดการ</th>
  </tr>

<?php
	include("connect2.php");
  include("dbConfig.php");
  $sql= "select * from member where level ='2' order by id_member";
  $result=mysqli_query($db,$sql);
  //for($i=0; $data=$sql->fetch(); $i++ );{
    while($data=mysqli_fetch_array($result, MYSQLI_ASSOC)){
    ?>
      <tr   style="font:bold 20px 'Aleo';font color="green" align="center" ">

            <td><?php echo $data['id_member'];  ?> </td>
            <td> <?php echo $data['m_username'];  ?> </td>
            <td> <?php echo $data['name'];  ?> </td>
            <td> <?php echo $data['email'];  ?> </td>
            <td> <?php echo $data['addr'];  ?> </td>
            <td> <?php echo $data['Tel'];  ?> </td>
            <td> <?php echo $data['level'];  ?> </td>
						<td align="center" style="background:#
            FFCCCC;">
							<a rel="facebox" title="Click to edit the product" href="deleteUser.php?id=<?php echo ($data['id_member'])?>"><img src="images/icon-bin.png" class="action" title="ลบ"/></a>
  </td>
  </tr>

<?php }?>
 </table>

<script type="text/javascript">
document.user.u_name.focus();
</script>

</body>
</html>
